﻿using employee_tesk_management.Data;
using employee_tesk_management.Hash;
using employee_tesk_management.HelperServices;
using employee_tesk_management.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;

namespace employee_tesk_management.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly AppDbContext _db;
        private readonly ILogger<EmployeeController> _logger;
        public EmployeeController(AppDbContext db, ILogger<EmployeeController> logger)
        {
            _db = db;
            _logger = logger;
        }
        public IActionResult Index()
        {
            var EmployeeList = _db.Employees.Where(x => x.IsDeleted == false).ToList();
            //ViewBag.EnumGender = new SelectList(Employees.Select(x=>x.Gender) );
            /*ViewBag.EnumGender = Enum.GetValues(typeof(EnumEmployeeGender));*/
            return View(EmployeeList);
        }


        public IActionResult addEmployee(Employee employee)
        {
            try
            {
                var emailList = _db.Employees.Where(x => x.Email == employee.Email).FirstOrDefault();

                if (emailList != null)
                {
                    TempData["EmialExists"] = true;
                }
                else
                {
                    Employee newEmp = new Employee();
                    newEmp.Name = employee.Name;
                    newEmp.Email = employee.Email;
                    newEmp.Gender = employee.Gender;
                    //newEmp.Status = employee.Status;
                    newEmp.EmployeeStage = employee.EmployeeStage;
                    newEmp.EmployeeType = employee.EmployeeType;
                    newEmp.EmployeeRole = employee.EmployeeRole;
                    newEmp.JoinDate = employee.JoinDate;
                    newEmp.ContractComletionDaate = employee.ContractComletionDaate;
                    newEmp.Password = PasswordEncryption.HashPassword(employee.Password);
                    newEmp.CreatedDate = DateTime.Now;
                    newEmp.JoinDate = employee.JoinDate;
                    newEmp.ContractComletionDaate = employee.ContractComletionDaate;
                    newEmp.ContactNo = employee.ContactNo;
                    newEmp.Address = employee.Address;
                    _db.Employees.Add(newEmp);
                    _db.SaveChanges();
                    TempData["EmialExists"] = false;
                }
                return RedirectToAction("Index", "Employee");
            }
            catch (Exception ex)
            {

                _logger.LogError(ex, "An error occurred while adding the employee. Please try again later.");
                return RedirectToAction("Error", "Error");
            }
        }
        public IActionResult DeletedEmployee()
        {
            try
            {
                var DeletedEmp = _db.Employees.Where(x => x.IsDeleted == true).ToList();
                return View(DeletedEmp);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error");
                return RedirectToAction("Error", "Error");  
            }
        }
        public IActionResult EditEmployeeData(Employee employee)
        {
            try
            {
                Employee oldData = _db.Employees.Find(employee.Id);
                oldData.Name = employee.Name;
                oldData.Address = employee.Address;
                oldData.Email = employee.Email;
                oldData.ContactNo = employee.ContactNo;
                oldData.Gender = employee.Gender;
                oldData.EmployeeType = employee.EmployeeType;
                oldData.EmployeeRole = employee.EmployeeRole;
                oldData.EmployeeStage = employee.EmployeeStage;
                oldData.JoinDate = employee.JoinDate;
                oldData.ContractComletionDaate = employee.ContractComletionDaate;
                oldData.UpdatedDate=DateTime.Now;
                if (employee.Password != null)
                {
                    if (employee.Password.Length < 5)
                    {
                        TempData["EditProfileMessage"] = "Password must be atleast 5 character";
                        return RedirectToAction("EmployeeEditProfile", "Employee", new { id = oldData.Id });
                    }
                    if(oldData.Password != PasswordEncryption.HashPassword(employee.Password)) 
                    {
                        oldData.Password = PasswordEncryption.HashPassword(employee.Password);
                    }
                }
                _db.SaveChanges();
                /*return RedirectToAction("EmployeeEditProfile", "Employee", new { id = oldData.Id });*/
                return RedirectToAction("Index", "Employee", new { id = oldData.Id });
            }
            catch(Exception ex) 
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

        public IActionResult EmployeeEditProfile(int id)
        {
            try
            {
                VMEmpData empData = new VMEmpData();
                empData.EmployeeList = _db.Employees.Where(x => x.Id == id).ToList();
                return View(empData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

        public IActionResult EmployeeProfileDetail(int id)
        {
            try
            {
                VMEmpData empData = new VMEmpData();
                empData.EmployeeList = _db.Employees.Where(x => x.Id == id).ToList();
                return View(empData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

        public IActionResult DeleteEmployee(int id)
        {
            try
            {
                Employee empData = _db.Employees.Find(id);
                empData.IsDeleted = true;
                _db.SaveChanges();
                return RedirectToAction("Index");
                

            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

        public  IActionResult UserDetails(int id)
        {
            try
            {
                var empdetails = _db.Employees.Include(x => x.Records).FirstOrDefault(x => x.Id == id);
                return View(empdetails);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }
        [HttpGet]
        public IActionResult ChangeEmployeePassword(int id)
        {
            try
            {
                Employee _ActiveUser = SessionServices.GetSession(HttpContext);
                VMValidatePassword UserData =new VMValidatePassword();
                UserData.EmployeeID= _ActiveUser.Id;
                UserData.EmployeeList=_db.Employees.Where(x=>x.Id == id).ToList();
                return View(UserData);

            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }
        [HttpPost]
        public IActionResult ChangeEmployeePassword(VMValidatePassword empData)
        {
            try
            {
                bool flag = true;
             Employee oldData =_db.Employees.Find(empData.EmployeeID);
             if(oldData.Password !=PasswordEncryption.HashPassword(empData.OldPassword)) 
                {
                    TempData["EditPasswordMessage"] = "Old password doesn't match";
                    flag = false;
                }
              else if(empData.ConfirmPassword !=empData.NewPassword) 
                {
                    TempData["EditPasswordMessage"] = "New password and confirm password doesn't match";
                    flag = false;
                }
                else
                {
                    oldData.Password = empData.ConfirmPassword;
                    _db.SaveChanges();
                    TempData["EditPasswordMessage"] = "Password changed successfully";
                    TempData["type"] = "success";
                }
                if (!flag)
                {
                    TempData["type"] = "danger";
                }
                return RedirectToAction("ChangeEmployeePassword", "Employee", new { id = empData.EmployeeID });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

    }
}
