﻿using employee_tesk_management.CustomAttributes;
using employee_tesk_management.Data;
using employee_tesk_management.HelperServices;
using employee_tesk_management.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace employee_tesk_management.Controllers
{

    public class UserTaskController : Controller
    {
        private readonly ILogger<UserTaskController> _logger;
        private readonly AppDbContext _db;
        private Employee _ActiveUser => SessionServices.GetSession(HttpContext);
        public UserTaskController(AppDbContext db, ILogger<UserTaskController> logger) 
        {
            _db = db;
            _logger = logger;
        }
        public IActionResult TaskList()
        {
            try
            {
                VMEmpData empData = new VMEmpData();
                empData.EmployeeList=_db.Employees.Where(x=>x.IsDeleted==false).ToList();
                empData.TaskList= _db.Record.Include(x=>x.Employee).OrderByDescending(x => x.CreatedDate).ToList();
                ViewBag.EmployeeList =new SelectList(empData.EmployeeList,"Id","Name");
                return View(empData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }
        public IActionResult ToDayTaskNew()
        {
            try
            {
                VMEmpData empData = new VMEmpData();
                empData.EmployeeList = _db.Employees.Where(x=>x.IsDeleted==false).ToList();
                empData.TaskList = _db.Record.Include(x=>x.Employee).OrderByDescending(x => x.CreatedDate).ToList();

                return View(empData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
        }

        [UserAuthorizationAttribute]
        public IActionResult EmployeeTask()
        {
            try
            {
                VMEmpData empData = new VMEmpData();
                DateTime currentDateTime = DateTime.Now;
                string formatedDate = currentDateTime.ToString("MM/dd/yyyy");
                List<Record> records = _db.Record.Where(x => x.EmployeeID == _ActiveUser.Id).ToList();
                empData.CanAddTask = true;
                foreach (var data in records)
                {
                    if (data.CreatedDate.ToString("MM/dd/yyyy").ToString() == formatedDate)
                    {
                        empData.CanAddTask = false;
                        break;
                    }
                }
                empData.EmployeeID = _ActiveUser.Id;
                empData.EmployeeName = _ActiveUser.Name;
                empData.EmployeeEmail = _ActiveUser.Email;
                empData.TaskList = _db.Record.Where(x => x.EmployeeID == _ActiveUser.Id).OrderByDescending(x=>x.CreatedDate).ToList();
                empData.EmployeeList = _db.Employees.Where(x => x.Id == _ActiveUser.Id).ToList();
                return View(empData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error occured while loading data");
                return RedirectToAction("Error","Error");
            }
            
        }

        public IActionResult addTask(Record record) 
        {
            try
            {
                Record recData = new Record();
                recData.Task = record.Task;
                recData.CreatedDate = DateTime.Now;
                recData.EmployeeID = _ActiveUser.Id;
                _db.Record.Add(recData);
                _db.SaveChanges();
                return RedirectToAction("EmployeeTask");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error","Error");
            }
         

        }

        public IActionResult editTask(Record record)
        {
            
            try
            {
                Record oldData = _db.Record.Find(record.Id);
                oldData.Task = record.Task;

                _db.SaveChanges();
                return RedirectToAction("EmployeeTask");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error","Error");
            }
            
        }

        public IActionResult deleteTask(int taskID) 
        {
            try
            {
                Record data = _db.Record.Find(taskID);
                _db.Record.Remove(data);
                _db.SaveChanges();
                return RedirectToAction("EmployeeTask");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }
            
        }

        public IActionResult UserDetail(int id)
        {
            try
            {
                var empDetail = _db.Employees.Include(x => x.Records).FirstOrDefault(x => x.Id == id);
                return View(empDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured while returing user details");
                return RedirectToAction("Error", "Error");
            }
           
        }

        public IActionResult toDayTask()
        {
            try
            {
                var emplist = _db.Employees.Where(x => x.IsDeleted == false).ToList();
                var nowDate = DateTime.Now;
                List<Record> records = _db.Record.Where(x => x.CreatedDate.Date == nowDate.Date).ToList();
                foreach (var item in emplist)
                {
                    var rec = records.FirstOrDefault(x => x.EmployeeID == item.Id);
                    item.Records.Add(rec == null ? new Record() : rec);
                }
                
                return View(emplist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured while loading employee task");
                return RedirectToAction("Error", "Error");
            }
            

        }
    }
}
