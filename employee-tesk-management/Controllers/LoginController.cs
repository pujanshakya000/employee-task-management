﻿using employee_tesk_management.Data;
using employee_tesk_management.Hash;
using employee_tesk_management.HelperServices;
using employee_tesk_management.Models;
using Microsoft.AspNetCore.Mvc;

namespace employee_tesk_management.Controllers
{
    public class LoginController : Controller
    {
        private readonly AppDbContext _db;
        private readonly IHttpContextAccessor _context;
        private readonly ILogger<LoginController> _logger;
        public LoginController(AppDbContext Db,ILogger<LoginController> logger)
        {
            _db = Db;
            _logger = logger;

        }
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult authenticateUser(string email, string password)
        {
            try
            {
                if (password != null && email != null)
                {
                    Employee employee = _db.Employees.FirstOrDefault(x => x.Email == email && x.IsDeleted == false);
                    if (employee == null)
                    {
                        TempData["ErrorMessage"] = "Incorrect Email";
                        return RedirectToAction("Login", "Login");
                    }
                    var passwordValue = employee.Password;
                    if (passwordValue != PasswordEncryption.HashPassword(password))
                    {
                        TempData["ErrorMessage"] = "Incorrect Password";
                        return RedirectToAction("Login", "Login");
                    }
                    SessionServices.SetSession(employee, HttpContext);
                    if(employee.IsAdmin)
                    {
                        return RedirectToAction("Dashboard", "Admin");
                    }
                    return RedirectToAction("EmployeeTask", "UserTask");

                }
                TempData["ErrorMessage"] = "Empty Fields";
                return RedirectToAction("Login", "Login");
            } 
            catch(Exception ex) 
            {
                _logger.LogError(ex, "An error pccured while login");
                return RedirectToAction("Error", "Error");
            }
            /*return RedirectToAction("Login", "Login");*/



        }

        public IActionResult Logout()
        {
            try
            {
                SessionServices.ClearSession(HttpContext);
                return RedirectToAction("Login", "Login");
            }
            catch(Exception ex) 
            {
                _logger.LogError(ex, "An error pccured while logout");
                return RedirectToAction("Error", "Error");
            }
            
        }
    }

   
}
