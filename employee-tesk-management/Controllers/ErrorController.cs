﻿using employee_tesk_management.Data;
using Microsoft.AspNetCore.Mvc;

namespace employee_tesk_management.Controllers
{
    public class ErrorController : Controller
    {
        private readonly AppDbContext _db;
        private readonly ILogger _logger;
        public ErrorController(AppDbContext db, ILogger logger)
        {
            _db = db;
            _logger = logger;
        }
        public IActionResult Error()
        {
            return View();
        }
         // TODO error log from serilog table
        /*public IActionResult ErrorLog()
        {
            var error = _logger.
            return View();
        }*/
    }
}
