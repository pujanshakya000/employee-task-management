﻿using employee_tesk_management.CustomAttributes;
using employee_tesk_management.Data;
using employee_tesk_management.Models;
using Microsoft.AspNetCore.Mvc;

namespace employee_tesk_management.Controllers
{
    [AdminAuthorization]
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;
        private readonly AppDbContext _db;
        public AdminController(AppDbContext db, ILogger<AdminController> logger)
        {
            _db = db;
            _logger = logger;
        }
        public IActionResult Dashboard()
        {
            try
            {
                VMEmpData Result = new VMEmpData();
                Result.TotalEmployee = _db.Employees.Where(x => x.IsDeleted == false).Count();
                Result.TotalIntern = _db.Employees.Where(x => x.EmployeeStage == EnumEmployeeStage.Internship && x.IsDeleted == false).Count();
                Result.TotalProvisionPeriod = _db.Employees.Where(x => x.EmployeeStage == EnumEmployeeStage.Probation_Period && x.IsDeleted == false).Count();
                Result.TotalContractual = _db.Employees.Where(x => x.EmployeeStage == EnumEmployeeStage.Contractual && x.IsDeleted == false).Count();
                Result.TotalAdmin = _db.Employees.Where(x => x.EmployeeType == EnumEmployeeType.Admin && x.IsDeleted == false).Count();
                Result.TotalDeveloper = _db.Employees.Where(x => x.EmployeeRole == EnumEmployeeRole.Developer && x.IsDeleted == false).Count();
                Result.TotalQA = _db.Employees.Where(x => x.EmployeeRole == EnumEmployeeRole.Support && x.IsDeleted == false).Count();
                Result.TotalMale = _db.Employees.Where(x => x.Gender == EnumEmployeeGender.Male && x.IsDeleted == false).Count();
                Result.TotalFemale = _db.Employees.Where(x => x.Gender == EnumEmployeeGender.Female && x.IsDeleted == false).Count();
                Result.EmployeeList = _db.Employees.Where(x => x.IsDeleted == false).ToList();
                return View(Result);
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error");
                return RedirectToAction("Error", "Error");
            }

           
        }
    }
}
