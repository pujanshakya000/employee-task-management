﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace employee_tesk_management.Models
{
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id{ get; set; }
        [Required]
        public string Name{ get; set; }
        [Required]
        public string Email{ get; set; }
        [Required]
        [MinLength(5)]
        public string Password{ get; set; }
        public bool IsDeleted { get; set; }
        public bool Status{ get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ContactNo { get; set; }
        public string Address {  get; set; }

        public EnumEmployeeGender Gender { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ContractComletionDaate { get; set; }

        public EnumEmployeeType EmployeeType { get; set; }
        public EnumEmployeeRole EmployeeRole { get; set; }
        public EnumEmployeeStage EmployeeStage { get; set; }    
        public ICollection<Record>Records { get; set; }

        public bool IsAdmin => EmployeeType == EnumEmployeeType.Admin;
        public bool IsUser => !IsAdmin;

        
    }

   
    public enum EnumEmployeeType
    {
        Admin =1,
        Employee =2
    }

    public enum EnumEmployeeStage
    {
        Internship=1,
        Probation_Period=2,
        Contractual=3
    }
    public enum EnumEmployeeRole
    {
        Admin=1,
        Developer=2,
        Support=3
    }

    public enum EnumEmployeeGender
    {
        Male = 1,
        Female = 2
    }
    public class VMEmpData
    {
        public bool CanAddTask { get; set; }
        public int? EmployeeID { get; set; }
        public string? EmployeeName { get; set; }
        public string? EmployeeEmail { get; set; }
        public int? TotalEmployee { get; set; }
        public int? TotalIntern { get; set; }
        public int? TotalProvisionPeriod { get; set; }
        public int? TotalContractual { get; set; }
        public int? TotalAdmin { get; set; }
        public int? TotalDeveloper { get; set; }
        public int? TotalQA { get; set; }
        public int? TotalMale { get; set; }
        public int? TotalFemale { get; set; }

        public IList<Record> TaskList { get; set; } = new List<Record>();
        public IList<Employee> EmployeeList { get; set; } = new List<Employee>();
    }

    public class VMValidatePassword
    {
        public int EmployeeID { get; set; }
        [Required]
        [MinLength(5)]
        public string OldPassword { get; set; }
        [Required]
        [MinLength(5)]
        public string NewPassword { get; set; }
        [Required]
        [MinLength(5)]
        public string ConfirmPassword { get; set; }
        public IList<Employee> EmployeeList { get; set; } = new List<Employee>();

    }

}
