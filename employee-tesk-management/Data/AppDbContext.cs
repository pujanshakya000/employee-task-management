﻿using employee_tesk_management.Models;
using Microsoft.EntityFrameworkCore;

namespace employee_tesk_management.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options): base(options)
        {
        }
        public DbSet<Employee>Employees { get; set; }
        public DbSet<Record> Record { get; set; }
    }
}
